class RedirectsController < ApplicationController
  def go
    redirect_url = Result.redirect_url(request.url)

    if redirect_url
      redirect_to(redirect_url, status: :moved_permanently)
    else
      head 404
    end
  end

  def query
    redirect_url = Result.redirect_url(params[:url])

    if redirect_url
      json_result = { redirect_url: redirect_url }
    else
      json_result = { error: "No redirect found" }
    end

    render json: json_result
  end
end
