class RulesController < ApplicationController
  def index
    @rules = Rule.order(priority: :desc)
  end

  def new
    @rule = Rule.new
  end

  def edit
    @rule = Rule.find(params[:id])
  end

  def create
    @rule = Rule.new(rule_params)

    if @rule.save
      flash[:success] = "Rule created successfully!"
      redirect_to(rules_path)
    else
      render(:new)
    end
  end

  def update
    @rule = Rule.find(params[:id])

    if @rule.update_attributes(rule_params)
      flash[:success] = "Rule updated successfully!"
      redirect_to(rules_path)
    else
      render(:edit)
    end
  end

  def destroy
    @rule = Rule.find(params[:id])

    if @rule.destroy
      flash[:success] = "Rule deleted successfully!"
      redirect_to(rules_path)
    else
      render(:edit)
    end
  end

  def test
    @results = Result.find(params[:url])
  end

  private

  def rule_params
    params.require(:rule).permit(
      :priority,
      :requested_url,
      :destination_url
    )
  end
end
