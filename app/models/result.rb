class Result
  attr_reader :url, :rule

  def self.redirect_url(url)
    last_updated_rule_timestamp = Rule.order(updated_at: :desc).first.updated_at.to_i
    cache_key = "#{last_updated_rule_timestamp}-#{url}"

    REDIRECT_CACHE.get(cache_key) || begin
      Rule.order(priority: :desc).find_each do |rule|
        result = Result.new(url, rule)
        if result.matches?
          REDIRECT_CACHE.set(cache_key, result.redirect_url)
          return result.redirect_url
        end
      end
    end
  end

  def self.find(url)
    results = []

    Rule.order(priority: :desc).find_each do |rule|
      result = Result.new(url, rule)
      results << result if result.matches?
    end

    results.reverse
  end

  def matches?
    match.present?
  end

  def match
    url.match(rule.requested_url)
  end

  def initialize(url, rule)
    @url = url
    @rule = rule
  end

  def redirect_url
    final_url = rule.destination_url

    while(placeholder_match = final_url.match(/\$(\d+)/))
      number = placeholder_match[1].to_i
      value = match[number].to_s
      final_url = final_url.gsub("$#{number}", value)
    end

    final_url
  end
end
