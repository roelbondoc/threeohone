class Rule < ApplicationRecord
  validates :priority, numericality: { only_integer: true }
  validates :requested_url, presence: true, uniqueness: true
  validates :destination_url, presence: true
end
