class AdminDomainConstraint
  def self.matches?(request)
    request.host == "threeohone.rwdev.io"
  end
end

Rails.application.routes.draw do
  constraints(AdminDomainConstraint) do
    resources :rules do
      collection do
        get "test"
      end
    end

    get "query", to: "redirects#query"
    root "rules#index"
  end

  match "*path", to: "redirects#go", via: :all, as: :go
end
