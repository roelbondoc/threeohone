require "rails_helper"

describe Result do
  let(:url) { double("url") }
  let(:rule) { double("rule") }
  let(:result) { described_class.new(url, rule) }

  subject { result }

  describe "#initialize" do
    it { expect(subject.url).to eq(url) }
    it { expect(subject.rule).to eq(rule) }
  end

  describe "#matches?" do
    let(:url) { "http://google.com" }

    context "when url matches the rule's requested_url" do
      let(:rule) { double("rule", requested_url: url) }
      it { expect(subject.matches?).to be true }
    end

    context "when url does not match the rule's requested_url" do
      let(:rule) { double("rule", requested_url: "nothing") }
      it { expect(subject.matches?).to be false }
    end
  end

  describe "#redirect_url" do
    let(:rule) { double("rule", destination_url: "Var1:$1 Var2:$2 Var3:$3") }

    before do
      allow(result).to receive(:match) { ["foo", "bar", "baz"] }
    end

    it { expect(subject.redirect_url).to eq("Var1:bar Var2:baz Var3:") }
  end
end
