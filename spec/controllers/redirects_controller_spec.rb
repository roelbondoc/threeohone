require "rails_helper"

describe RedirectsController do
  describe "#query" do
    context "when a result is found" do
      let!(:rule) { Rule.create(priority: 1, requested_url: "http://google.com", destination_url: "http://gmail.com") }
      it "should return the redirect" do
        get :query, params: { url: "http://google.com" }
        expect(response.body).to eq("{\"redirect_url\":\"http://gmail.com\"}")
      end
    end

    context "when a result is not found" do
      let!(:rule) { Rule.create(priority: 1, requested_url: "http://hotmail.com", destination_url: "http://msn.com") }
      it "should return an error" do
        get :query, params: { url: "http://windows.com" }
        expect(response.body).to eq("{\"error\":\"No redirect found\"}")
      end
    end
  end
end
