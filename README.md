# Three Oh One

## Requirements

* docker
* docker-compose
* ability to bind to port 80 on host machine

## Setup

1. `docker-compose up`
2. `docker-compose run --rm web bundle exec rake db:create`
3. `docker-compose run --rm web bundle exec rake db:migrate`
4. Update host file

```
127.0.0.1 threeohone.rwdev.io
127.0.0.1 written.rwdev.io
127.0.0.1 videos.rwdev.io
```

5. Navigate to `threeohone.rwdev.io` to access the admin interface
6. Test the rules by visiting any of the test urls
7. (Optional to load some sample rules) `docker-compose run --rm web bundle exec rake db:seed`

## Web Admin Features

* Ability to add/edit/delete rules
* Testing input box to test urls

## Notes

* There is a simple test suite using rspec. It's not full test coverage, but I just wanted to cover some of the more complicated parts.
